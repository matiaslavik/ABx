/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "SkillDragIcon.h"
#include "UIPriorities.h"

void SkillDragIcon::RegisterObject(Context* context)
{
    context->RegisterFactory<SkillDragIcon>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

SkillDragIcon::SkillDragIcon(Context* context) :
    Window(context)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    Texture2D* tex = cache->GetResource<Texture2D>("Textures/UI.png");
    SetLayout(LM_HORIZONTAL);
    SetLayoutBorder(IntRect(4, 4, 4, 4));
    SetTexture(tex);
    SetImageRect(IntRect(48, 0, 64, 16));
    SetBorder(IntRect(4, 4, 4, 4));
    SetMinSize(60, 60);
    SetMaxSize(60, 60);
    SetPriority(Priorities::DragElement);
}

SkillDragIcon::~SkillDragIcon()
{ }

void SkillDragIcon::SetSkill(const UIElement& elem, Texture* tex)
{
    BorderImage* icon = CreateChild<BorderImage>();
    icon->SetTexture(tex);
    SetPosition(elem.GetPosition());
    SetVar("SkillIndex", elem.GetVar("SkillIndex"));
    SetVar("SkillPos", elem.GetVar("SkillPos"));
}
