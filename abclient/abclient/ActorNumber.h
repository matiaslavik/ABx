/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <Urho3DAll.h>

class Actor;

class ActorNumber : public Text
{
    URHO3D_OBJECT(ActorNumber, Text)
public:
    enum class NumberType
    {
        None,
        LostHealth,
        LostEnergy,
        GainedHealth,
        GainedEnergy,
        XP
    };
    static void RegisterObject(Context* context);

    ActorNumber(Context* context);
    ~ActorNumber() override;

    void SetNumber(NumberType type, int value, bool isPlayer);
    void SetTarget(SharedPtr<Actor> owner);
private:
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    WeakPtr<Actor> target_;
    NumberType type_{ NumberType::None };
    int value_{ 0 };
    float timeVisible_{ 0.0f };
    int screenPosX_{ 0 };
};
