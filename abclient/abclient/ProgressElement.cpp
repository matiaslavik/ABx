/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "ProgressElement.h"

static constexpr sa::color PROGRESS_COLOR = { 0, 0, 0, 160 };
static constexpr int HEIGHT = 10;

void ProgressElement::RegisterObject(Context* context)
{
    context->RegisterFactory<ProgressElement>();
    URHO3D_COPY_BASE_ATTRIBUTES(BorderImage);
}

ProgressElement::ProgressElement(Context* context) :
    BorderImage(context)
{
    texture_ = MakeShared<Texture2D>(context_);
    texture_->SetSize(32, HEIGHT, Graphics::GetRGBAFormat(), TEXTURE_DYNAMIC);
    texture_->SetNumLevels(1);
    texture_->SetMipsToSkip(QUALITY_LOW, 0);
    image_ = MakeShared<Image>(context_);
    image_->SetSize(32, HEIGHT, 4);
    bitmap_.set_bitmap(32, HEIGHT, 4, image_->GetData());
    SetTexture(texture_);
    SetFullImageRect();
    SubscribeToEvent(E_RENDERUPDATE, URHO3D_HANDLER(ProgressElement, HandleRenderUpdate));
    SetVisible(false);
}

ProgressElement::~ProgressElement()
{
}

void ProgressElement::Start()
{
    if (mode_ == Mode::Inc)
        current_ = 0.0f;
    else
        current_ = max_;
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ProgressElement, HandleUpdate));
    SetVisible(true);
}

void ProgressElement::Stop()
{
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(ProgressElement, HandleUpdate));
    if (mode_ == Mode::Inc)
        current_ = 0.0f;
    else
        current_ = max_;
    current_ = 0.0f;
    SetVisible(false);
}

void ProgressElement::SetMaxTicks(uint32_t ticks)
{
    max_ = (float)ticks / 1000.0f;
}

void ProgressElement::HandleUpdate(StringHash, VariantMap& eventData)
{
    using namespace Update;
    if (mode_ == Mode::Inc)
    {
        current_ += eventData[P_TIMESTEP].GetFloat();
        if (current_ >= max_)
            Stop();
    }
    else
    {
        current_ -= eventData[P_TIMESTEP].GetFloat();
        if (current_ <= 0.0f)
            Stop();
    }
}

void ProgressElement::HandleRenderUpdate(StringHash, VariantMap&)
{
    if (!IsVisible())
        return;
    bitmap_.clear();
    DrawProgress();
    texture_->SetData(image_, true);
}

void ProgressElement::DrawProgress()
{
    const float percent = current_ / max_;
    if (direction_ == Direction::Backward)
        bitmap_.draw_rectangle(bitmap_.width() - (int)(bitmap_.width() * percent), 0, bitmap_.width(), bitmap_.height(), PROGRESS_COLOR);
    else
        bitmap_.draw_rectangle(0, 0, (int)(bitmap_.width() * percent), bitmap_.height(), PROGRESS_COLOR);
}
