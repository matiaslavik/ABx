/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "HeroWindow.h"
#include "Shortcuts.h"
#include "UIPriorities.h"
#include "ActorResourceBar.h"
#include "LevelManager.h"
#include "Player.h"

void HeroWindow::RegisterObject(Context* context)
{
    context->RegisterFactory<HeroWindow>();
    URHO3D_COPY_BASE_ATTRIBUTES(Window);
}

HeroWindow::HeroWindow(Context* context) :
    Window(context)
{
    SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* file = cache->GetResource<XMLFile>("UI/HeroWindow.xml");
    LoadXML(file->GetRoot());
    SetName("HeroWindow");

    SetPriority(Priorities::StandardWindow);

    Shortcuts* scs = GetSubsystem<Shortcuts>();
    Text* caption = GetChildStaticCast<Text>("CaptionText", true);
    caption->SetText(scs->GetCaption(Events::E_SC_TOGGLEHERODWINDOW, "Hero", true));

    auto* xpContainer = GetChild("XPContainer", true);
    xpBar_ = xpContainer->CreateChild<ActorXPBar>();

    UIElement* container = GetChild("Container", true);
    tabgroup_ = container->CreateChild<TabGroup>();
    tabgroup_->SetSize(container->GetSize());
    tabgroup_->SetPosition(0, 0);

    tabgroup_->SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());
    tabgroup_->SetAlignment(HA_CENTER, VA_TOP);
    tabgroup_->SetColor(Color(0, 0, 0, 0));
    tabgroup_->SetStyleAuto();
    {
        TabElement* elem = CreateTab(tabgroup_, "General");
        CreatePageGeneral(elem);
    }
    tabgroup_->SetEnabled(true);

    SetVisible(true);

    SetStyleAuto();
    UpdateLayout();

    SubscribeEvents();
}

HeroWindow::~HeroWindow()
{
    UnsubscribeFromAllEvents();
}

void HeroWindow::UpdateAll()
{
    auto* lm = GetSubsystem<LevelManager>();
    auto* player = lm->GetPlayer();
    if (!player)
        return;

    auto* heroInfoContainer = GetChild("HeroInfoContanier", true);
    Text* nameText = heroInfoContainer->GetChildStaticCast<Text>("CharacterNameText", true);
    nameText->SetText(player->name_);

    LineEdit* spEdit = heroInfoContainer->GetChildStaticCast<LineEdit>("SPEdit", true);
    spEdit->SetText(String(player->stats_.sp));

    xpBar_->SetActor(SharedPtr<Player>(player));
}

void HeroWindow::SubscribeEvents()
{
    Button* closeButton = GetChildStaticCast<Button>("CloseButton", true);
    SubscribeToEvent(closeButton, E_RELEASED, URHO3D_HANDLER(HeroWindow, HandleCloseClicked));
    SubscribeToEvent(Events::E_LEVELREADY, URHO3D_HANDLER(HeroWindow, HandleLevelReady));
    SubscribeToEvent(Events::E_OBJECTPROGRESS, URHO3D_HANDLER(HeroWindow, HandleObjectProgress));
}

void HeroWindow::HandleCloseClicked(StringHash, VariantMap&)
{
    SetVisible(false);
}

void HeroWindow::HandleLevelReady(StringHash, VariantMap&)
{
    UpdateAll();
}

void HeroWindow::HandleObjectProgress(StringHash, VariantMap&)
{
    UpdateAll();
}

void HeroWindow::LoadWindow(Window* wnd, const String& fileName)
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* xml = cache->GetResource<XMLFile>(fileName);
    wnd->LoadXML(xml->GetRoot());
    // It seems this isn't loaded from the XML file
    wnd->SetLayoutMode(LM_VERTICAL);
    wnd->SetLayoutBorder(IntRect(4, 4, 4, 4));
    wnd->SetPivot(0, 0);
    Texture2D* tex = cache->GetResource<Texture2D>("Textures/UI.png");
    wnd->SetTexture(tex);
    wnd->SetImageRect(IntRect(48, 0, 64, 16));
    wnd->SetBorder(IntRect(4, 4, 4, 4));
    wnd->SetImageBorder(IntRect(0, 0, 0, 0));
    wnd->SetResizeBorder(IntRect(8, 8, 8, 8));
}

TabElement* HeroWindow::CreateTab(TabGroup* tabs, const String& page)
{
    static const IntVector2 tabSize(65, 20);
    static const IntVector2 tabBodySize(500, 380);

    ResourceCache* cache = GetSubsystem<ResourceCache>();
    TabElement* tabElement = tabs->CreateTab(tabSize, tabBodySize);
    tabElement->tabText_->SetFont(cache->GetResource<Font>("Fonts/ClearSans-Regular.ttf"), 10);
    tabElement->tabText_->SetText(page);
    tabElement->tabBody_->SetDefaultStyle(GetSubsystem<UI>()->GetRoot()->GetDefaultStyle());

    tabElement->tabBody_->SetImageRect(IntRect(48, 0, 64, 16));
    tabElement->tabBody_->SetLayoutMode(LM_VERTICAL);
    tabElement->tabBody_->SetPivot(0, 0);
    tabElement->tabBody_->SetPosition(0, 0);
    tabElement->tabBody_->SetStyleAuto();
    return tabElement;
}

void HeroWindow::CreatePageGeneral(TabElement* tabElement)
{
    BorderImage* page = tabElement->tabBody_;

    Window* wnd = page->CreateChild<Window>();
    LoadWindow(wnd, "UI/HeroPageGeneral.xml");
    page->UpdateLayout();
}
