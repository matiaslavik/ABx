include("/scripts/includes/immutable.lua")

function enum(tbl)
  -- Enum like thing. First element is 0
  local length = #tbl
  for i = 1, length do
    local v = tbl[i]
    tbl[v] = i - 1
  end
  return readonlytable(tbl)
end
