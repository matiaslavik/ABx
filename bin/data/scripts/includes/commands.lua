include("/scripts/includes/enum.lua")

-- Keep in synch with AB::GameProtocol::CommandType
Command = enum {
  "Unknown",
  "ChatGeneral",
  "ChatGuild",
  "ChatParty",
  "ChatTrade",
  "ChatWhisper",
  "Resign",
  "Stuck",
  "Age",
  "Deaths",
  "Health",
  "Xp",
  "Pos",
  "Roll",
  "Sit",
  "Stand",
  "Cry",
  "Taunt",
  "Ponder",
  "Wave",
  "Laugh",
  "Shrug",
  "Phew",
  "Sigh",
  "PetName",
}
