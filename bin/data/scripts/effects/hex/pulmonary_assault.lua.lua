-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/attributes.lua")
include("/scripts/includes/damage.lua")
include("/scripts/includes/consts.lua")

isPersistent = false

index = 5510
name = "Pulmonary Assault"
icon = "Textures/Skills/Pulmonary Assault.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryHex

function onStart(source, target)
  return true
end

function onEndUseSkill(source, target, skill)
  if (skill:IsType(SkillTypeShout) or skill:IsType(SkillTypeChant) or skill:IsType(SkillTypeEcho)) then
    source:KnockDown(self.Source, 0)
  end
end
