-- By Koudelka

include("/scripts/includes/skill_consts.lua")
include("/scripts/includes/consts.lua")
include("/scripts/includes/attributes.lua")

isPersistent = false

index = 5328
name = "Dance of the Dead"
icon = "Textures/Skills/Dance of the Dead.png"
soundEffect = ""
particleEffect = ""
category = EffectCategoryEnchantment

function onStart(source, target)
  return true
end

function onSpawnedSlave(source, slave)
  if (slave.SlaveKind == SLAVE_KIND_MINION) then
    slave:AddSpeed(0.5)
  end
end
