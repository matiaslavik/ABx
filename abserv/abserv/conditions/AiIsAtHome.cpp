/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "AiIsAtHome.h"
#include "../Game.h"
#include "../Npc.h"
#include <abshared/Mechanic.h>
#include <abscommon/Utils.h>
#include <abscommon/Logger.h>

namespace AI {
namespace Conditions {

IsAtHome::IsAtHome(const ArgumentsType& arguments) :
    Condition(arguments)
{
    GetArgument<Game::Range>(arguments, 0, range_);
    if (static_cast<size_t>(range_) >= Utils::CountOf(Game::RangeDistances))
    {
        LOG_ERROR << "Range has an impossible value " << static_cast<size_t>(range_) << std::endl;
        range_ = Game::Range::Touch;
    }
}

bool IsAtHome::Evaluate(Agent& agent, const Node&)
{
    const auto& npc = GetNpc(agent);
    bool haveHome = !npc.GetHomePos().Equals(Math::Vector3::Zero);
    if (!haveHome)
        return true;
    if (range_ == Game::Range::Map)
        return true;

    return npc.GetHomePos().Distance(npc.GetPosition()) < Game::RangeDistances[static_cast<size_t>(range_)];
}

}
}
