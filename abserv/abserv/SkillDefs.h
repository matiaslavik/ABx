/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

namespace Game {

enum SkillEffect : uint32_t
{
    SkillEffectNone = 0,
    SkillEffectResurrect = 1 << 1,
    SkillEffectHeal = 1 << 2,
    SkillEffectProtect = 1 << 3,
    SkillEffectDamage = 1 << 4,
    SkillEffectSpeed = 1 << 5,
    SkillEffectInterrupt = 1 << 6,
    SkillEffectGainEnergy = 1 << 7,
    SkillEffectRemoveEnchantment = 1 << 8,
    SkillEffectRemoveHex = 1 << 9,
    SkillEffectRemoveStance = 1 << 10,
    SkillEffectCounterInterrupt = 1 << 11,
    SkillEffectCounterKD = 1 << 12,
    SkillEffectBlock = 1 << 13,
    SkillEffectSummonMinion = 1 << 14,
    SkillEffectSnare = 1 << 15,
    SkillEffectEnergyLoss = 1 << 16,
    SkillEffectRemoveCondition = 1 << 17,
    SkillEffectIncAttributes = 1 << 18,
    SkillEffectKnockDown = 1 << 19,
    SkillEffectFasterCast = 1 << 20,
    SkillEffectDisableSkill = 1 << 21,
    SkillEffectDecAttributes = 1 << 22,
};

enum SkillEffectTarget : uint32_t
{
    SkillTargetNone = 0,
    SkillTargetSelf = 1 << 1,
    SkillTargetTarget = 1 << 2,              // Selected target
    SkillTargetAoe = 1 << 3,
    SkillTargetParty = 1 << 4,
};

// Skills may or may not require a target
enum SkillTargetType : uint32_t
{
    SkillTargetTypeNone = 0,
    SkillTargetTypeSelf = 1,
    SkillTargetTypeAllyAndSelf = 2,
    SkillTargetTypeAllyWithoutSelf = 3,
    SkillTargetTypeFoe = 4,
    SkillTargetTypeAny = 5
};

enum class CostType
{
    Activation,
    Energy,
    Adrenaline,
    HpSacrify,
    Recharge,
};

}