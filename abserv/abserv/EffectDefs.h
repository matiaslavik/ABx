/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <AB/Entities/Effect.h>

namespace Game {

class Effect;

enum EffectAttr : uint8_t
{
    EffectAttrId = 1,
    EffectAttrTicks,

    // For serialization
    EffectAttrEnd = 254
};

enum class EffectCategories : uint32_t
{
    None = 0,
    // From skills -------------------------------------------------------------
    Condition = (1 << AB::Entities::EffectCondition) - 1,
    Enchantment = (1 << AB::Entities::EffectEnchantment) - 1,
    Hex = (1 << AB::Entities::EffectHex) - 1,
    Shout = (1 << AB::Entities::EffectShout) - 1,
    Spirit = (1 << AB::Entities::EffectSpirit) - 1,
    Ward = (1 << AB::Entities::EffectWard) - 1,
    Well = (1 << AB::Entities::EffectWell) - 1,
    Skill = (1 << AB::Entities::EffectSkill) - 1,
    Preparation = (1 << AB::Entities::EffectPreparation) - 1,
    Stance = (1 << AB::Entities::EffectStance) - 1,
    Form = (1 << AB::Entities::EffectForm) - 1,
    Glyphe = (1 << AB::Entities::EffectGlyphe) - 1,
    PetAttack = (1 << AB::Entities::EffectPetAttack) - 1,
    WeaponSpell = (1 << AB::Entities::EffectWeaponSpell) - 1,
    Signet = (1 << AB::Entities::EffectSignet) - 1,
};

// Effects are fist-in-last-out
using EffectList = ea::vector<ea::shared_ptr<Effect>>;

}