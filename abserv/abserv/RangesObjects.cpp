/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "RangesObjects.h"

namespace Game {

void RangesObjects::Add(Range range, uint32_t id)
{
    if (range < Range::Map)
        objects_[static_cast<size_t>(range)].emplace(id);
}

void RangesObjects::Clear()
{
    for (auto& r : objects_)
        r.clear();
}

bool RangesObjects::Contains(Range range, uint32_t id) const
{
    if (range < Range::Map)
    {
        const auto& r = objects_[static_cast<size_t>(range)];
        return r.contains(id);
    }
    if (range == Range::Map)
        return true;
    return false;
}

Range RangesObjects::GetRangeOfObject(uint32_t id) const
{
    Range result = Range::Map;
    // When an object is in a range, it is also in all other closer ranges
    VisitRangesReverse([id, &result, this](Range range, const auto& objects)
    {
        if (objects.contains(id))
        {
            result = range;
            return Iteration::Break;
        }
        return Iteration::Continue;
    });
    return result;
}

}
