/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <AB/Entities/Entity.h>
#include <vector>

namespace AB {
namespace Entities {

// Player inventory size
inline constexpr size_t DEFAULT_INVENTORY_SIZE = 40;

enum class CharacterSex : uint8_t
{
    Unknown,
    Female,
    Male
};

enum DeathStatIndex : size_t
{
    DeathStatIndexCount,
    DeathStatIndexAtXp,
    __DeathStatIndexCount
};

struct Character : Entity
{
    MAKE_ENTITY(Character)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(name);
        s.value(profession);
        s.value(profession2);
        s.value(professionUuid);
        s.value(profession2Uuid);
        s.value(level);
        s.value(pvp);
        s.value(xp);
        s.value(skillPoints);
        s.value(sex);
        s.value(accountUuid);
        s.value(modelIndex);
        s.value(skillTemplate);
        s.value(onlineTime);
        s.value(deletedTime);
        s.value(creation);
        s.value(currentMapUuid);
        s.value(lastOutpostUuid);

        s.value(lastLogin);
        s.value(lastLogout);
        s.value(instanceUuid);
        s.value(partyUuid);
        s.value(inventorySize);
        s.value(deathStats);
        s.value(companion);
    }

    std::string name;
    std::string profession;
    std::string profession2;
    std::string professionUuid{ EMPTY_GUID };
    std::string profession2Uuid{ EMPTY_GUID };
    uint8_t level{ 0 };
    /// PvP only character
    bool pvp{ false };
    uint32_t xp{ 0 };
    uint32_t skillPoints{ 0 };
    CharacterSex sex{ CharacterSex::Unknown };
    std::string currentMapUuid{ EMPTY_GUID };
    std::string lastOutpostUuid{ EMPTY_GUID };
    std::string accountUuid{ EMPTY_GUID };
    // Index in game_items
    uint32_t modelIndex{ 0 };
    std::string skillTemplate;

    int64_t onlineTime{ 0 };
    /// 0 if not deleted
    timestamp_t deletedTime{ 0 };
    timestamp_t creation{ 0 };

    timestamp_t lastLogin{ 0 };
    timestamp_t lastLogout{ 0 };
    /// ID of AB::Entities::GameInstance
    std::string instanceUuid{ EMPTY_GUID };
    /// AB::Entities::Party
    std::string partyUuid{ EMPTY_GUID };
    uint16_t inventorySize{ DEFAULT_INVENTORY_SIZE };
    std::string deathStats;
    std::string companion;
};

typedef std::vector<Character> CharList;

}
}
