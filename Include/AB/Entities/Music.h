/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum MusicStyle : uint32_t
{
    MusicStyleUnknown = 0,
    MusicStyleEastern = 1 << 1,
    MusicSttyleWestern = 1 << 2,
    MusicStyleSlow = 1 << 3,
    MusicStyleDriving = 1 << 4,
    MusicStyleEpic = 1 << 5,
    MusicStyleAction = 1 << 6,
    MusicStyleMystic = 1 << 7,
    MusicStyleIntense = 1 << 8,
    MusicStyleAggressive = 1 << 9,
    MusicStyleRelaxed = 1 << 10,
    MusicStyleHumorous = 1 << 11
};

struct Music : Entity
{
    MAKE_ENTITY(Music)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(mapUuid);
        s.value(localFile);
        s.value(remoteFile);
        s.value(sorting);
        s.value(style);
    }

    std::string mapUuid;
    std::string localFile;
    std::string remoteFile;
    uint8_t sorting = 0;
    MusicStyle style = MusicStyleUnknown;
};

}
}
