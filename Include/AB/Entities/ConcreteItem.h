/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

enum class StoragePlace : uint8_t
{
    None = 0,
    Scene = 1,      // On ground
    Inventory,      // In players inventory
    Chest,          // Account chest
    Equipped,       // A player has equipped this item
    Merchant,       // Merchant has this item and may be bought by a player
};

enum ConcreteItemFlag : uint32_t
{
    ConcreteItemFlagCustomized = 1,
};

struct ConcreteItem : Entity
{
    MAKE_ENTITY(ConcreteItem)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(playerUuid);
        s.value(accountUuid);
        s.value(upgrade1Uuid);
        s.value(upgrade2Uuid);
        s.value(upgrade3Uuid);
        s.value(itemUuid);
        s.value(itemStats);

        s.value(storagePlace);
        s.value(storagePos);
        s.value(count);
        s.value(creation);
        s.value(deleted);
        s.value(value);
        s.value(instanceUuid);
        s.value(mapUuid);
        s.value(flags);
        s.value(sold);
    }

    std::string playerUuid{ EMPTY_GUID };
    std::string accountUuid{ EMPTY_GUID };
    StoragePlace storagePlace{ StoragePlace::None };
    uint16_t storagePos{ 0 };
    std::string upgrade1Uuid{ EMPTY_GUID };
    std::string upgrade2Uuid{ EMPTY_GUID };
    std::string upgrade3Uuid{ EMPTY_GUID };
    std::string itemUuid{ EMPTY_GUID };
    std::string itemStats;
    uint32_t count{ 0 };
    timestamp_t creation{ 0 };
    timestamp_t deleted{ 0 };
    uint16_t value{ 0 };
    std::string instanceUuid{ EMPTY_GUID };
    std::string mapUuid{ EMPTY_GUID };
    uint32_t flags{ 0 };
    timestamp_t sold{ 0 };
};

inline bool IsItemCustomized(uint32_t flags)
{
    return (flags & ConcreteItemFlagCustomized) == ConcreteItemFlagCustomized;
}

}
}
