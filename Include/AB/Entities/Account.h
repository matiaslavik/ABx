/**
 * Copyright 2017-2020 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <AB/Entities/Entity.h>

namespace AB {
namespace Entities {

inline const uint32_t ACCOUNT_DEF_CHARSLOTS = 6;
// Account chest size
inline constexpr size_t DEFAULT_CHEST_SIZE = 80;

enum class AccountType : uint8_t
{
    Unknown = 0,
    Normal = 1,
    Tutor = 2,
    SeniorTutor = 3,
    Gamemaster = 4,
    God = 5
};

enum AccountStatus : uint8_t
{
    AccountStatusUnknown = 0,
    AccountStatusActivated = 1,
    AccountStatusDeleted = 2
};

enum OnlineStatus : uint8_t
{
    OnlineStatusOffline = 0,
    OnlineStatusAway,
    OnlineStatusDoNotDisturb,
    OnlineStatusOnline,
    OnlineStatusInvisible              // Like offline for other users
};

inline bool IsOnline(OnlineStatus status)
{
    // OnlineStatusOfflineis used internally. The user can use OnlineStatusInvisible
    // to appear to others as offline.
    return status != OnlineStatusOffline && status != OnlineStatusInvisible;
}

struct Account : Entity
{
    MAKE_ENTITY(Account)
    template<typename S>
    void Serialize(S& s)
    {
        Entity::Serialize(s);
        s.value(type);
        s.value(status);
        s.value(creation);
        s.value(name);
        s.value(password);
        s.value(email);
        s.value(authToken);
        s.value(authTokenExpiry);
        s.value(currentServerUuid);
        s.value(charSlots);
        s.value(currentCharacterUuid);
        s.value(onlineStatus);
        s.value(guildUuid);
        s.value(clientPubKey);
        s.value(characterUuids);
    }

    AccountType type = AccountType::Unknown;
    AccountStatus status = AccountStatusUnknown;
    timestamp_t creation = 0;
    std::string name;
    std::string password;
    std::string email;
    std::string authToken;
    timestamp_t authTokenExpiry;
    /// The server currently logged in. Required for cross server chat etc.
    std::string currentServerUuid = EMPTY_GUID;
    uint32_t charSlots = ACCOUNT_DEF_CHARSLOTS;
    /// Last or current character
    std::string currentCharacterUuid = EMPTY_GUID;
    std::vector<std::string> characterUuids;
    OnlineStatus onlineStatus = OnlineStatusOffline;
    std::string guildUuid = EMPTY_GUID;
    // Clients DH public key (16 byte)
    std::array<uint8_t, 16> clientPubKey;
    uint16_t chest_size = DEFAULT_CHEST_SIZE;
};

}
}
