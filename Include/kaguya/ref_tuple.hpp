// Copyright satoren
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

#include "kaguya/config.hpp"

namespace kaguya {

template <typename RefTuple, typename GetTuple> struct ref_tuple {
  RefTuple tref;
  ref_tuple(const RefTuple &va) : tref(va) {}
  void operator=(const FunctionResults &fres) {
    tref = fres.get_result(types::typetag<GetTuple>());
  }
  template <class T> void operator=(const T &fres) { tref = fres; }
};
template <class... Args>
ref_tuple<standard::tuple<Args &...>, standard::tuple<Args...> >
tie(Args &... va) {
  typedef standard::tuple<Args &...> RefTuple;
  typedef standard::tuple<Args...> GetTuple;
  return ref_tuple<RefTuple, GetTuple>(RefTuple(va...));
}
}
