# dbtool

Database administration tool. Since this program writes directly to the database
server, the data server must not run.

IMPORTANT: Whenever you use the `dbtool` the server must not be running. `dbtool`
directly writes to the database, and when the data server runs it may overwrite
what `dbtool` wrote.
