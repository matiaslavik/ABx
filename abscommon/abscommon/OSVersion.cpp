/**
 * Copyright 2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "OSVersion.h"
#include <sa/Compiler.h>
#ifdef SA_PLATFORM_WIN
#include <Windows.h>
#include <lm.h>
#pragma comment(lib, "netapi32.lib")
#elif defined(SA_PLATFORM_LINUX)
#include <sys/utsname.h>
#include <sstream>
#endif

namespace System {

#ifdef SA_PLATFORM_WIN
static bool GetWinMajorMinorVersion(DWORD& major, DWORD& minor)
{
    bool result = false;
    LPBYTE pinfoRawData = 0;
    if (NERR_Success == ::NetWkstaGetInfo(NULL, 100, &pinfoRawData))
    {
        WKSTA_INFO_100* pworkstationInfo = (WKSTA_INFO_100*)pinfoRawData;
        major = pworkstationInfo->wki100_ver_major;
        minor = pworkstationInfo->wki100_ver_minor;
        ::NetApiBufferFree(pinfoRawData);
        result = true;
    }
    return result;
}
static std::string GetWindowsVersionString()
{
    std::string     result;
    OSVERSIONINFOEX osver;
    SYSTEM_INFO     sysInfo;
    typedef void(__stdcall* GETSYSTEMINFO) (LPSYSTEM_INFO);

    PRAGMA_WARNING_PUSH;
    PRAGMA_WARNING_DISABLE_MSVC(4996);
    memset(&osver, 0, sizeof(osver));
    osver.dwOSVersionInfoSize = sizeof(osver);
    GetVersionEx((LPOSVERSIONINFO)&osver);
    PRAGMA_WARNING_POP;
    DWORD major = 0;
    DWORD minor = 0;
    if (GetWinMajorMinorVersion(major, minor))
    {
        osver.dwMajorVersion = major;
        osver.dwMinorVersion = minor;
    }
    else if (osver.dwMajorVersion == 6 && osver.dwMinorVersion == 2)
    {
        OSVERSIONINFOEXW osvi;
        ULONGLONG cm = 0;
        cm = VerSetConditionMask(cm, VER_MINORVERSION, VER_EQUAL);
        ZeroMemory(&osvi, sizeof(osvi));
        osvi.dwOSVersionInfoSize = sizeof(osvi);
        osvi.dwMinorVersion = 3;
        if (VerifyVersionInfoW(&osvi, VER_MINORVERSION, cm))
        {
            osver.dwMinorVersion = 3;
        }
    }

    GETSYSTEMINFO getSysInfo = (GETSYSTEMINFO)GetProcAddress(GetModuleHandle(L"kernel32.dll"), "GetNativeSystemInfo");
    if (getSysInfo == NULL)
        getSysInfo = ::GetSystemInfo;
    getSysInfo(&sysInfo);

    if (osver.dwMajorVersion == 10 && osver.dwMinorVersion >= 0 && osver.wProductType != VER_NT_WORKSTATION) result = "Windows 10 Server";
    if (osver.dwMajorVersion == 10 && osver.dwMinorVersion >= 0 && osver.wProductType == VER_NT_WORKSTATION) result = "Windows 10";
    if (osver.dwMajorVersion ==  6 && osver.dwMinorVersion == 3 && osver.wProductType != VER_NT_WORKSTATION) result = "Windows Server 2012 R2";
    if (osver.dwMajorVersion ==  6 && osver.dwMinorVersion == 3 && osver.wProductType == VER_NT_WORKSTATION) result = "Windows 8.1";
    if (osver.dwMajorVersion ==  6 && osver.dwMinorVersion == 2 && osver.wProductType != VER_NT_WORKSTATION) result = "Windows Server 2012";
    if (osver.dwMajorVersion ==  6 && osver.dwMinorVersion == 2 && osver.wProductType == VER_NT_WORKSTATION) result = "Windows 8";
    if (osver.dwMajorVersion ==  6 && osver.dwMinorVersion == 1 && osver.wProductType != VER_NT_WORKSTATION) result = "Windows Server 2008 R2";
    if (osver.dwMajorVersion ==  6 && osver.dwMinorVersion == 1 && osver.wProductType == VER_NT_WORKSTATION) result = "Windows 7";
    if (osver.dwMajorVersion ==  6 && osver.dwMinorVersion == 0 && osver.wProductType != VER_NT_WORKSTATION) result = "Windows Server 2008";
    if (osver.dwMajorVersion ==  6 && osver.dwMinorVersion == 0 && osver.wProductType == VER_NT_WORKSTATION) result = "Windows Vista";
    if (osver.dwMajorVersion ==  5 && osver.dwMinorVersion == 2 && osver.wProductType == VER_NT_WORKSTATION && sysInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64) result = "Windows XP x64";
    if (osver.dwMajorVersion ==  5 && osver.dwMinorVersion == 2)                                             result = "Windows Server 2003";
    if (osver.dwMajorVersion ==  5 && osver.dwMinorVersion == 1)                                             result = "Windows XP";
    if (osver.dwMajorVersion ==  5 && osver.dwMinorVersion == 0)                                             result = "Windows 2000";
    if (osver.dwMajorVersion < 5)                                                                            result = "unknown";

    if (osver.wServicePackMajor != 0)
    {
        std::string sp;
        char buf[128] = { 0 };
        sp = " Service Pack ";
        sprintf_s(buf, sizeof(buf), "%hd", osver.wServicePackMajor);
        sp.append(buf);
        result += sp;
    }

    return result;
}
#endif

#ifdef SA_PLATFORM_LINUX
static std::string GetLinuxVersionString()
{
    std::stringstream ss;
    utsname u;
    uname(&u);
    ss << u.sysname << " " << u.release << " " << u.version;
    return ss.str();
}
#endif

std::string GetOSVersionString()
{
#if defined(SA_PLATFORM_WIN)
    return GetWindowsVersionString();
#elif defined(SA_PLATFORM_LINUX)
    return GetLinuxVersionString();
#else
    return "Unknown";
#endif
}

}
