/**
 * Copyright 2017-2021 Stefan Ascher
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <string>
#include <map>
#include "Servers.h"
#include <sa/Iteration.h>

namespace HTTP {

class Cookie
{
public:
    enum class SameSite
    {
        Lax = 0,
        Strict,
        None
    };
private:
    std::string content_;
    std::string domain_;
    std::string path_{ "/" };
    time_t expires_;
    bool httpOnly_{ false };
    SameSite sameSite_{ SameSite::Lax };
    bool secure_{ false };
public:
    static void RegisterLua(kaguya::State& state);
    Cookie();
    explicit Cookie(const std::string& content);
    ~Cookie() = default;

    const std::string& GetContent() const { return content_; }
    void SetContent(const std::string& value) { content_ = value; }
    const std::string& GetDomain() const { return domain_; }
    void SetDomain(const std::string& value) { domain_ = value; }
    const std::string& GetPath() const { return path_; }
    void SetPath(const std::string& value) { path_ = value; }
    time_t GetExpires() const { return expires_; }
    void SetExpires(time_t value) { expires_ = value; }
    bool IsHttpOnly() const { return httpOnly_; }
    void SetHttpOnly(time_t value) { httpOnly_ = value; }
    SameSite GetSameSite() const { return sameSite_; }
    void SetSameSite(SameSite value) { sameSite_ = value; }
    bool IsSecure() const { return secure_; }
    void SetSecure(time_t value) { secure_ = value; }
};

class Cookies
{
private:
    std::map<std::string, std::unique_ptr<Cookie>> cookies_;
public:
    static void RegisterLua(kaguya::State& state);
    Cookies() = default;
    explicit Cookies(const HttpsServer::Request& request);
    void Write(SimpleWeb::CaseInsensitiveMultimap& header);
    Cookie* Add(const std::string& name);
    Cookie* Get(const std::string& name);
    void Delete(const std::string& name);
    template<typename Callback>
    void VisitCookies(Callback&& callback)
    {
        for (const auto& cookie : cookies_)
        {
            if (cookie.second)
            {
                if (callback(cookie.first, *cookie.second.get()) == Iteration::Break)
                    break;
            }
        }
    }
};

}
