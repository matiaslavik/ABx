INSERT INTO public.game_items VALUES ('e3445d68-4484-4fbb-ac18-a61e256287b7', 24, 'Cheetah', '', 'Objects/Cheetah.xml', '', 1, 0, 0, 17, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/cheetah.lua', 0);
INSERT INTO public.game_items VALUES ('197a6c76-006b-4d43-b66d-ffb9e0b89a35', 25, 'Tiger', '', 'Objects/Tiger.xml', '', 1, 0, 0, 17, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/cheetah.lua', 0);
INSERT INTO public.game_items VALUES ('faa12704-4b4e-4741-b936-49448b0dc992', 26, 'Turtle', '', 'Objects/Turtle.xml', '', 1, 0, 0, 20, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/turtle.lua', 0);
INSERT INTO public.game_items VALUES ('2057b4e9-2b48-4d7c-8da3-88650a7d50c5', 27, 'Goat', '', 'Objects/Goat.xml', '', 1, 0, 0, 19, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/goat.lua', 0);
INSERT INTO public.game_items VALUES ('acd616d2-ee31-4db3-9083-28dfeba4ba86', 28, 'Horse', '', 'Objects/Horse.xml', '', 1, 0, 0, 18, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/horse.lua', 0);
INSERT INTO public.game_items VALUES ('96df1778-32d1-4b55-9cf6-474751d65000', 29, 'Donkey', '', 'Objects/Donkey.xml', '', 1, 0, 0, 21, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/donkey.lua', 0);
INSERT INTO public.game_items VALUES ('f3df452c-a91e-4c5a-9d91-77e6b3a23261', 30, 'Lion female', '', 'Objects/LionFemale.xml', '', 1, 0, 0, 17, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/lion_female.lua', 0);
INSERT INTO public.game_items VALUES ('9e9b97c1-bbb0-4866-831c-55686a045f17', 31, 'Lion male', '', 'Objects/LionMale.xml', '', 1, 0, 0, 17, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/lion_male.lua', 0);
INSERT INTO public.game_items VALUES ('519f38b1-6268-4fad-9b6b-17fc1e5ae801', 32, 'Camel', '', 'Objects/Camel.xml', '', 1, 0, 0, 23, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/camel.lua', 0);

INSERT INTO public.game_items VALUES ('1440f73b-e869-464a-a6da-6d47e3690712', 33, 'Bull', '', 'Objects/Bull1.xml', '', 1, 0, 0, 0, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/bull.lua', 0);
INSERT INTO public.game_items VALUES ('70ec1b0d-c378-4a1c-b8be-db5128e01c67', 34, 'Cow', '', 'Objects/Cow.xml', '', 1, 0, 0, 0, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/cow.lua', 0);
INSERT INTO public.game_items VALUES ('7e8a63a9-bafa-43fa-ba88-0c2e5b373bd2', 35, 'Bear', '', 'Objects/Bear1.xml', '', 1, 0, 0, 25, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/bear.lua', 0);
INSERT INTO public.game_items VALUES ('df9f17a8-49a6-4335-ac16-f32d32d5aa55', 36, 'Sheep', '', 'Objects/Sheep.xml', '', 1, 0, 0, 0, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/sheep.lua', 0);
INSERT INTO public.game_items VALUES ('b2287bee-ead1-4aa0-9e57-8f4c1b504611', 37, 'Spot', '', 'Objects/Spot.xml', '', 1, 0, 0, 24, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/mini_pets/spot.lua', 0);
INSERT INTO public.game_items VALUES ('d54e320f-60c1-4d59-bfa3-fb665ad77e90', 38, 'Spot Mini', '/scripts/items/spot_mini.lua', 'Objects/Spot.xml', 'Textures/Icons/Items/SpotMini.png', 1003, 0, 0, 0, '00000000-0000-0000-0000-000000000000', '', 52);
INSERT INTO public.game_items VALUES ('e48940e5-f76f-42a4-abd6-6b9cd7017dd5', 39, 'Crocodile', '', 'Objects/Crocodile.xml', '', 1, 0, 0, 0, '00000000-0000-0000-0000-000000000000', '/scripts/actors/npcs/crocodile.lua', 0);

UPDATE public.game_items SET model_class = 22 WHERE idx = 19;

UPDATE public.versions SET value = value + 1 WHERE name = 'game_items';

UPDATE public.versions SET value = 78 WHERE name = 'schema';
