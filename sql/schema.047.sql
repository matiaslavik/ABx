INSERT INTO public.game_skills VALUES (public.random_guid(), 201, 'Glyph of Concentration', '00000000-0000-0000-0000-000000000000', 32, 0, 'Glyph: Your next spell can not be interrupted for 15 seconds.', 'Glyph: 15 seconds. Your next spell can not be interrupted.', 'Textures/Skills/placeholder.png', '/scripts/skills/glyph_of_concentration.lua', '85d0ef81-50f4-11e8-a7ca-02100700d6f0', '', '', 1, 1000, 10000, 5, 0, 0, 0, 0);
UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects VALUES (public.random_guid(), 201, 'Glyph of Concentration', 11, '/scripts/effects/glyph/glyph_of_concentration.lua', 'Textures/Skills/placeholder.png');
UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 47 WHERE name = 'schema';
