INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('452aff8c-3205-4a59-a20d-78a916320f32', 5056, '	/scripts/skills/water_magic/hailstorm.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('b7369683-cb72-44ee-844e-117521062119', 5057, '/scripts/skills/water_magic/healing_rain.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('8a1ee6ab-5a59-4b8f-9ec0-94b594a74f84', 5058, '/scripts/skills/water_magic/ice_hammer.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('a06b1d9e-ffe7-46d9-adfc-daf79f5fd5dc', 5059, '/scripts/skills/water_magic/mind_wash.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('828568cf-0ebe-46f3-be3f-36fa61b3d54b', 5060, '/scripts/skills/water_magic/polar_vortex.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('9f6c2d7a-0237-4323-800a-3387b8829d84', 5061, '/scripts/skills/water_magic/ride_the_wave.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('167eceaa-493f-4274-ab42-30d18fa24538', 5062, '/scripts/skills/water_magic/shatter_flesh.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('758440ad-ad3e-4cf9-8b70-327d5187af63', 5063, '/scripts/skills/water_magic/snowblind.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('c3ad7528-dfa9-43c0-a8b6-05719e72c34b', 5064, '/scripts/skills/water_magic/tsunami.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('2b587651-3d54-48f9-8d6d-815d6fe0e505', 5065, '/scripts/skills/water_magic/winter_blast.lua', 1);
INSERT INTO public.game_skills (uuid, idx, script, access) VALUES ('0c5e797d-6be9-4085-b5b0-7004cadf5adc', 5066, '/scripts/skills/water_magic/winter_rose.lua', 1);

UPDATE public.versions SET value = value + 1 WHERE name = 'game_skills';

INSERT INTO public.game_effects (uuid, idx, script) VALUES ('0f2f0908-df26-4564-9a70-803af5d2ebcf', 5058, '/scripts/effects/hex/ice_hammer.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('cde96dcb-7368-4a6f-a985-2f53c3d821af', 5062, '/scripts/effects/hex/shatter_flesh.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('2e2ecaf7-3995-4798-9703-b3cec4f83b2d', 1003, '/scripts/effects/general/slowdown.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('1c891389-038f-4b51-957d-1708eda5fcd8', 5063, '/scripts/effects/skill/snowblind.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('88d63c6b-6ada-4f4d-ac82-8e7a14287443', 5065, '/scripts/effects/hex/winter_blast.lua');
INSERT INTO public.game_effects (uuid, idx, script) VALUES ('89fe4ef7-af4a-41e4-b282-bc1e50db2cf4', 5066, '/scripts/effects/hex/winter_rose.lua');

UPDATE public.versions SET value = value + 1 WHERE name = 'game_effects';

UPDATE public.versions SET value = 67 WHERE name = 'schema';
